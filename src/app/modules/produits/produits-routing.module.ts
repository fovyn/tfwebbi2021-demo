import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateFormComponent } from 'src/app/modules/produits/components/create-form/create-form.component';
import { ListComponent } from 'src/app/modules/produits/components/list/list.component';
import { ProduitsComponent } from './produits.component';

const routes: Routes = [
  { path: '', component: ProduitsComponent, children: [
    { path: 'add', component: CreateFormComponent },
    { path: 'list', component: ListComponent }
  ]}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProduitsRoutingModule { }
