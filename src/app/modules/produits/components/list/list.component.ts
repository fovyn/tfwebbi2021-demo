import { Component, OnInit } from '@angular/core';
import { Produit, Produits } from 'src/app/modules/produits/models/produit.model';
import { ProduitService } from 'src/app/modules/produits/services/produit.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  private _items: Produits = [];
  get items(): Produits { return this._items; } 

  constructor(private $service: ProduitService) { }

  ngOnInit(): void {
    const addEvent$ = this.$service.getAddEvent$();
    addEvent$.subscribe((produit: Produit) => this._updateList(produit));
    this.$service
      .findAllProduct()
      .subscribe((produits: Produits) => this._items = produits);
  }

  private _updateList(produit: Produit) {
    this._items.push(produit);
  }
}
