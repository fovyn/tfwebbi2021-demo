import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FORM_PRODUITS_CREATE } from 'src/app/modules/produits/forms/produit.form';
import { ProduitService } from 'src/app/modules/produits/services/produit.service';

@Component({
  selector: 'app-create-form',
  templateUrl: './create-form.component.html',
  styleUrls: ['./create-form.component.scss']
})
export class CreateFormComponent implements OnInit {
  fg = new FormGroup(FORM_PRODUITS_CREATE);

  constructor(private $service: ProduitService) { }

  ngOnInit(): void {
  }

  submitAction() {
    if (this.fg.valid) {
      console.log(this.fg.value);
      const produit = this.fg.value;
      this.$service.addProduct(produit);
    }
  }
}
