import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { Produit, Produits } from 'src/app/modules/produits/models/produit.model';
import { environment } from 'src/environments/environment';

const {api_url} = environment;
const [src1, src2] = ["Hello", "Blop"]

@Injectable({
  providedIn: 'root'
})
export class ProduitService {
  private _addEvent$ = new Subject<Produit>();
  getAddEvent$(): Observable<Produit> { return this._addEvent$.asObservable(); }

  constructor(private $http: HttpClient) {
  }

  findAllProduct(): Observable<Produits> {
    return this.$http.get<Produits>(`${api_url}/produits`);
  }

  async addProduct(produit: Produit): Promise<Produit> {
    const newProduit = await this.$http
      .post<Produit>(`${api_url}/produits`, produit).toPromise();

    this._addEvent$.next(newProduit);

    return newProduit;
  }
}
