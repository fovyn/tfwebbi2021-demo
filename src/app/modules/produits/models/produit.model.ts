export type Produit = {id?: number, titre: string, description?: string, prix: number};

export type Produits = Array<Produit>;