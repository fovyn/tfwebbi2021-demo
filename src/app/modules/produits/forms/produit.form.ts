import { FormControl, Validators } from "@angular/forms";

export const FORM_PRODUITS_CREATE = {
    "titre": new FormControl(null, [Validators.required]),
    "description": new FormControl(null, []),
    "prix": new FormControl(0, [Validators.required, Validators.min(0)])
};