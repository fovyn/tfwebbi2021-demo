import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProduitsRoutingModule } from './produits-routing.module';
import { ProduitsComponent } from './produits.component';
import { ListComponent } from './components/list/list.component';
import { CreateFormComponent } from './components/create-form/create-form.component';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    ProduitsComponent,
    ListComponent,
    CreateFormComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    ProduitsRoutingModule
  ]
})
export class ProduitsModule { }
