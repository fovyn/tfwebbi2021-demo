import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/modules/users/services/user.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  constructor(private $http: HttpClient, private $userService: UserService) { }

  ngOnInit(): void {
    // this.$userService.user$.
    this.$userService.subscribe((user: any) => console.log("LIST", user));

    const call = async () => {
      const data = await this.$http
        .get("http://localhost:3000/users")
        .toPromise();
      console.log(data);
    }
    
    call().then(_ => console.log("FINISHED"));
  }

}
