import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/modules/users/services/user.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  constructor(private $userService: UserService) { }

  ngOnInit(): void {
    this.$userService.subscribe((user: any) => console.log("USERS", user));
  }

  loginAction() {
    this.$userService.login();
  }
}
