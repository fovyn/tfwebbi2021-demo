import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private _user$ = new BehaviorSubject<any>(null);

  subscribe(action: any) {
    this._user$.subscribe(action);
  }

  get user$(): Observable<any> { return this._user$.asObservable(); }
  

  constructor() { }

  login() {
    this._user$.next({username: 'Flavian', password: 'Blop'});
  }
}
