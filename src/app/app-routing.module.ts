import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { 
    path: 'blop', 
    loadChildren: () => import('./modules/users/users.module').then(m => m.UsersModule)
  },
  { path: 'produits', loadChildren: () => import('./modules/produits/produits.module').then(m => m.ProduitsModule) }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
